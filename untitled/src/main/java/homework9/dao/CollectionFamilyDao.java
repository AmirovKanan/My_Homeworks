package homework9.dao;

import java.util.List;
import java.util.Optional;

public interface CollectionFamilyDao<K> {
    List<K> getAllFamilies();
    Optional<K> getFamilyByIndex(int index);
    boolean deleteFamily(K family);
    boolean deleteFamily(int index);
    K saveFamily(K family);

}
