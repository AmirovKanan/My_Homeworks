package homework9.additional;

public enum Species {
        Cat,
        Dog,
        Eagle,
        UNKNOWN,
        Fish,
        DomesticCat,
        RoboCat;
}
