package homework9.additional;

import java.util.Map;

public final class Man extends Human {
    public Man(String name, String surname, int year, int iq){
        super(name, surname, year, iq);
    }

    @Override
    public void greetPet() {
        System.out.println("I am man, I love my pet:)");
    }

    public void repairCar(){
        System.out.println("I repaired my car!!");
    }
}
