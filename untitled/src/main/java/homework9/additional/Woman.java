package homework9.additional;

import java.util.Map;

public final class Woman extends Human {
    public Woman(String name, String surname, int year, int iq){
        super(name, surname, year, iq);
    }

    @Override
    public void greetPet() {
        System.out.println("I am woman, how cute is that!!");
    }

    public void makeup(){
        System.out.println("This is my makeup)");
    }
}
