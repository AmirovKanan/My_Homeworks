package homework9;

import homework9.additional.*;
import homework9.controller.FamilyController;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        FamilyController familyController = new FamilyController();

        Man father1 = new Man("Leonardo", "DiCaprio", 1974, 85);
        Woman mother1 = new Woman("Camila", "Morrone", 1977, 78);
        Human child1 = new Human("Michael", "Karleone", 1997, 85);
        Pet domesticCat = new DomesticCat("Tommy", 8, 75);

        Man father2 = new Man("Mayk", "Tayson", 1974, 85);
        Woman mother2 = new Woman("Lady", "Karleone", 1977, 78);
        Human child2 = new Human("Jane", "Addison", 1997, 85);
        Pet fish = new Fish("Fish", 2, 23);

        Man father3 = new Man("Mayk", "Tayson", 1974, 85);
        Woman mother3 = new Woman("Addy", "Smith", 1977, 78);
        Human child3 = new Human("Oliver", "Khan", 1997, 85);
        Pet dog = new Dog("Max", 7, 86);

        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother2, father2);
        familyController.createNewFamily(mother3, father3);

        familyController.adoptChild(familyController.getFamilyById(0), child1);
        familyController.adoptChild(familyController.getFamilyById(1), child2);
        familyController.adoptChild(familyController.getFamilyById(2), child3);

        familyController.addPet(0,domesticCat);
        familyController.addPet(1, fish);
        familyController.addPet(2,dog);

        familyController.bornChild(familyController.getFamilyById(0), "masculine");



        familyController.displayAllFamilies();

        System.out.println();

        familyController.getFamiliesLessThan(2);
        familyController.getFamiliesMoreThan(1);
        familyController.countFamiliesWithMemberCount(4);
        familyController.deleteFamilyByIndex(2);
        familyController.countFamiliesWithMemberCount(4);
        familyController.getFamilyById(1);
        System.out.println(familyController.getPets(0));

    }
}

