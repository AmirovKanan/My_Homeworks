package homework8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Family {
    private Woman mother;
    private Man father;
    private List<Human> children;
    private ArrayList<Pet> pets;
    private int numberOfChildren;
    private int numberOfAddedChild = 0;

    public Family(Woman mother, Man father, int numberOfChildren, ArrayList<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<Human>();
        this.numberOfChildren = numberOfChildren;
        this.pets = pets;
    }


    public void setPet(Pet pet) {
        this.pets = pets;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public Woman getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public ArrayList<Pet> getPet() {
        return pets;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public boolean addChild(Human child) {
        if (numberOfAddedChild >= this.numberOfChildren) {
            // throw new ArrayIndexOutOfBoundsException("Family is full");
            return false;
        } else {
            children.add(child);
            numberOfAddedChild++;
            return true;
        }
    }


//    public boolean deleteChild(int indexOfChildToDelete) {
//
//        int count = 0;
//        if (indexOfChildToDelete > this.index || indexOfChildToDelete < 0)
//            return false;
//        Human[] childrenArrayAfterDeletion = new Human[this.children.size() - 1];
//
//        for (int index = 0; index < children.size(); index++) {
//            if (index == indexOfChildToDelete)
//                continue;
//            childrenArrayAfterDeletion[count++] = this.children.get(index);
//        }
//        this.children = Arrays.asList(childrenArrayAfterDeletion);
//        index--;
//        return true;
//    }

//    public boolean deleteChildObject(Human child) {
//        number_Delete_Object = -1;
//        if (countFamily() - 2 != 0){
//            for (int i = 0; i < children.size(); i++) {
//                if (children.get(i).equals(child)) {
//                    number_Delete_Object = i;
//                    break;
//                }
//            }
//        }
//        return deleteChild(number_Delete_Object);
//    }


    public String Children() {
        String allChildren = "";
        for (Human child : children) {
            allChildren += child.toString();
        }
        return allChildren;
    }

    public int countFamily() {
        return 2 + children.size(); // 2 is Parents count
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalize worked");
    }

}