package homework8;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {

    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;
    private Species species;

    //Creating Constructors
    public Pet() {
        this.species = Species.UNKNOWN;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }
    //Methods(return)
    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    //Methods (void)
    public void eat() {
        System.out.println("I am eating..");
    }

    public abstract void respond(String nickname);

    public void foul() {
        System.out.println("I need to cover it up");
    }
    // Equals and HashCode

    @Override
    public String toString() {
        return "Pet{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalize worked");
    }
}

