package homework8;

import java.util.Set;

public class DomesticCat extends Pet {

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.DomesticCat);
    }

    @Override
    public void respond(String nickname) { System.out.println("My name is " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("I did foul..");
    }
}
