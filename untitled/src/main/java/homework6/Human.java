package homework6;

import java.util.Arrays;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
  //  private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;

    //Creating constructor


    public Human(String name, String surname, int year, int iq)
    {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

//    public Human(String name, String surname, int year, Human mother, Human father)
//    {
//        this.name = name;
//        this.surname = surname;
//        this.year = year;
//        this.mother = mother;
//        this.father = father;
//    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
       // this.pet = pet;
//        this.mother = mother;
//        this.father = father;
        this.schedule = schedule;
    }

    //Methods
//    public void greetPet()
//    {
//        System.out.printf("Hello, %s", pet.getNickname());
//    }
//
//    public void describePet()
//    {
//        String slyness;
//        if(pet.getTrickLevel() > 50)
//        {
//            slyness = "very sly";
//        }
//        else slyness = "almost not sly";
//
//        System.out.printf("I have a %s, he is %d years old, he is %s", pet.getSpecies(), pet.getAge(), slyness);
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    // Equals and hashcode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Arrays.equals(schedule, human.schedule);
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Human human = (Human) o;
//        return year == human.year &&
//                iq == human.iq &&
//                name.equals(human.name) &&
//                surname.equals(human.surname) &&
////                mother.equals(human.mother) &&
////                father.equals(human.father) &&
//                Arrays.equals(schedule, human.schedule);
//    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, mother, father);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    public String schedule(){
        String s = "";
        for (int i = 0; i < schedule.length; i++) {
            s += "{";
            s += this.schedule[i][0];
            s += "---";
            s += this.schedule[i][1];
            //  s += "---";
            //  s += schedule[i][2];
            s += "}";
        }
        return s;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule() +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalize worked");
    }


}