package homework6;

public class Main {

    public static void main(String[] args) {
        String[] habbits = {"playing", "walking", "eating"};
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                                {DayofWeek.Monday.name(), "Family Time"},
                                {DayofWeek.Tuesday.name(), "Gym"},
                                {DayofWeek.Wednesday.name(), "Shopping"},
                                {DayofWeek.Thursday.name(),"Walkig"},
                                {DayofWeek.Friday.name(), "Playing football"},
                                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };

        Human father = new Human("Leonardo", "DiCaprio", 1974, 85, schedule);
        Human mother = new Human("Camila", "Morrone", 1977, 78, schedule);
        Pet pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        Human child = new Human("Michael", "Karleone", 1997, 85, schedule);
        Family family = new Family(mother, father,pet, 2);

        System.out.println("Number of family is " + family.countFamily());

        System.out.println(family.addChild(child) ? "Child added" : "Not added");

        Human child2 = new Human("Muhammad", "Ali", 1956, 95, schedule);

        System.out.println(family.addChild(child2) ? "Child added" : "Not added");


        System.out.println("Number of family is " + family.countFamily() + "\n");

        System.out.println(family.toString());
       // System.out.println(pet.toString());
       // System.out.println(child.toString());
        System.out.println();

        System.out.println("Delete child by Index:");
        System.out.println(family.deleteChild(1) ? "Child deleted" : "There is no child to delete");
        System.out.println("Number of family is " + family.countFamily() + "\n");

        System.out.println("Delete child by Object:");
        System.out.println(family.deleteChildObject(child) ? "Child deleted" : "Child was not deleted;");
        System.out.println("Number of family is " + family.countFamily() + "\n");
        //System.out.println(family.deleteChild(1) ? "Child deleted" : "There is no child to delete");




        System.out.println("Testing Equals and Hascode methodes: ");

        System.out.println("child = father --> " + child.equals(father));
        System.out.println("child = child --> " + child.equals(child));


        System.out.println("HashCode of child --> " + child.hashCode());

        System.out.println("HashCode of mother --> " + mother.hashCode() + "\n");

        System.gc();

    }
}

