package homework7;

public class RoboCat extends Pet {

    public RoboCat(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.RoboCat);
    }

    @Override
    public void respond(String nickname) { System.out.println("My name is " + this.getNickname());
    }

    @Override
    public void foul() {

    }
}
