package homework7;

public class Fish extends Pet {

    public Fish(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.Fish);
    }

    @Override
    public void respond(String nickname) { System.out.println("My name is " + this.getNickname());
    }

    @Override
    public void foul() {

    }
}
