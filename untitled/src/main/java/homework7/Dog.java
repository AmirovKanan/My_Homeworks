package homework7;

public class Dog extends Pet {

    public Dog(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.Dog);
    }

    @Override
    public void respond(String nickname) { System.out.println("My name is " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("I did foul..");
    }
}
