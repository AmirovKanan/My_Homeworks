package homework7;

public final class Man extends Human{
    public Man(String name, String surname, int year, int iq, String[][] schedule){
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        System.out.println("I am man, I love my pet:)");
    }

    public void repairCar(){
        System.out.println("I repaired my car!!");
    }
}
