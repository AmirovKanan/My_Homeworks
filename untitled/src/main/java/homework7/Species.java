package homework7;

public enum Species {
        Cat,
        Dog,
        Eagle,
        UNKNOWN,
        Fish,
        DomesticCat,
        RoboCat;
}
