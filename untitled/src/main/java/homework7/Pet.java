package homework7;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {

    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits = new String[3];
    private Species species;

    //Creating Constructors
    public Pet() {
        this.species = Species.UNKNOWN;
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public void setSpecies(Species species) {
        this.species = species;
    }

    //Methods(return)
    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    //Methods (void)
    public void eat() {
        System.out.println("I am eating..");
    }

    public abstract void respond(String nickname);

    public void foul() {
        System.out.println("I need to cover it up");
    }

    // Equals and HashCode

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                nickname.equals(pet.nickname) &&
                Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalize worked");
    }

}

