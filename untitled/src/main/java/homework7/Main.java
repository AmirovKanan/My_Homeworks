package homework7;

public class Main {

    public static void main(String[] args) {

        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                                {DayofWeek.Monday.name(), "Family Time"},
                                {DayofWeek.Tuesday.name(), "Gym"},
                                {DayofWeek.Wednesday.name(), "Shopping"},
                                {DayofWeek.Thursday.name(),"Walkig"},
                                {DayofWeek.Friday.name(), "Playing football"},
                                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };


        String[] habitsOfFish = {"eating", "swim"};

        String[] habitsOfDomesticCat = {"playing", "walking", "eating"};

        String[] habitsOfDog = {"playing", "eating", "barking"};

        String[] habitsOfRoboCat = {"speaking", "walking"};


        Pet dog = new Dog("Max", 8, 81, habitsOfDog);
        Pet domesticCat = new DomesticCat("Tom", 9, 87, habitsOfDomesticCat);
        Pet fish = new Fish("Red", 1, 25, habitsOfFish);
        Pet roboCat = new RoboCat("RoboTom", 4, 100, habitsOfRoboCat);

        Man father = new Man("Leonardo", "DiCaprio", 1974, 85, schedule);
        Woman mother = new Woman("Camila", "Morrone", 1977, 78, schedule);
      //  Pet pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        Human child = new Human("Michael", "Karleone", 1997, 85, schedule);
        Family family = new Family(mother, father, 2, domesticCat);



        System.out.println(family.addChild(child) ? "Child added" : "Not added");

        System.out.println(family.toString());

        System.out.println();

        mother.greetPet();
        mother.makeup();
        System.out.println();
        father.greetPet();
        father.repairCar();

//
//        System.out.println("Delete child by Index:");
//        System.out.println(family.deleteChild(1) ? "Child deleted" : "There is no child to delete");
//        System.out.println("Number of family is " + family.countFamily() + "\n");
//
//        System.out.println("Delete child by Object:");
//        System.out.println(family.deleteChildObject(child) ? "Child deleted" : "Child was not deleted;");
//        System.out.println("Number of family is " + family.countFamily() + "\n");
//        //System.out.println(family.deleteChild(1) ? "Child deleted" : "There is no child to delete");

//        System.out.println("Testing Equals and Hascode methodes: ");
//
//        System.out.println("child = father --> " + child.equals(father));
//        System.out.println("child = child --> " + child.equals(child));
//
//
//        System.out.println("HashCode of child --> " + child.hashCode());
//
//        System.out.println("HashCode of mother --> " + mother.hashCode() + "\n");
//
//        System.gc();

    }
}

