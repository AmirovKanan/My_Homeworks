package homework7;

public final class Woman extends Human{
    public Woman(String name, String surname, int year, int iq, String[][] schedule){
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        System.out.println("I am woman, how cute is that!!");
    }

    public void makeup(){
        System.out.println("This is my makeup)");
    }
}
