package homework7;
import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Woman mother;
    private Man father;
    private Human[] children;
    private Pet pet;
    private int numberOfChildren;
    private int index = 0;
    private int number_Delete_Object = 0;

    public Family(Woman mother, Man father, int numberOfChildren, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[numberOfChildren];
        this.numberOfChildren = numberOfChildren;
        this.pet = pet;
    }


    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public Woman getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public boolean addChild(Human child) {
        if (numberOfChildren > children.length) {
            // throw new ArrayIndexOutOfBoundsException("Family is full");
            return false;
        } else {
            children[index++] = child;
           // System.out.println(index);
            return true;
        }

    }


    public boolean deleteChild(int indexOfChildToDelete) {

        int count = 0;
        if (indexOfChildToDelete > this.index || indexOfChildToDelete < 0)
            return false;
        Human[] childrenArrayAfterDeletion = new Human[this.children.length - 1];

        for (int index = 0; index < children.length; index++) {
            if (index == indexOfChildToDelete)
                continue;
            childrenArrayAfterDeletion[count++] = this.children[index];
        }
        this.children = childrenArrayAfterDeletion;
        index--;
        return true;
    }

    public boolean deleteChildObject(Human child) {
        number_Delete_Object = -1;
        if (countFamily() - 2 != 0){
            for (int i = 0; i < children.length; i++) {
                if (children[i].equals(child)) {
                    number_Delete_Object = i;
                    break;
                }
            }
        }
        return deleteChild(number_Delete_Object);
    }


    public String Children() {
        String allChildren = "";
        for (Human child : children) {
            allChildren += child.toString();
        }
        return allChildren;
    }

    public int countFamily() {
        return 2 + index; // 2 is Parents count
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    // Equals and hashcode

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return numberOfChildren == family.numberOfChildren &&
                index == family.index &&
                mother.equals(family.mother) &&
                father.equals(family.father) &&
                Arrays.equals(children, family.children) &&
                pet.equals(family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet, numberOfChildren, index);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                " mother=" + mother +
                ",\nfather=" + father +
              //  ",\nchildren=" + Children() +
                ", \npet=" + pet +
                //", \nnumberOfChildren=" + numberOfChildren +
                // ", index=" + index +
                "}";
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalize worked");
    }

}