package homework4;

public class Main {
    public static void main(String[] args) {
        String [] habbits = {"playing","walking","eating"};
        String[][] schedule = {{"Sunday", "Meditation"},
                {"Monday", "Family Time"},
                {"Tuesday", "Gym"},
                {"Wednesday", "Shopping"},
                {"Thursday","Walkig"},
                {"Friday", "Playing football"},
                {"Saturday", "Hanging out with friends"}
        };
        Human father = new Human("Leonardo", "DiCaprio", 1974);
        Human mother = new Human("Camila", "Morrone", 1977);

        Pet pet = new Pet("Cat", "Tom",4, 90, habbits );
        Human child = new Human("Michael", "Karleone", 1997, 85, pet, mother, father, schedule);

        System.out.println(pet.toString());
        System.out.println(child.toString());

    }
}
