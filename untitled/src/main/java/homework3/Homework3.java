package homework3;

import java.util.Scanner;

public class Homework3 {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];

        //Filling matrix
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";

        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";

        scedule[2][0] = "Tuesday";
        scedule[2][1] = "do exercise";

        scedule[3][0] = "Wednesday";
        scedule[3][1] = "Stay at home";

        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to cinema";

        scedule[5][0] = "Friday";
        scedule[5][1] = "hangout with friends";

        scedule[6][0] = "Saturday";
        scedule[6][1] = "go to nightclub";


        Scanner sc = new Scanner(System.in);

        // Printing appropriate plan of the weekday
        while (true) {
            System.out.println("Please, input the day of the week: ");
            String weekday = sc.nextLine();

            if (weekday.equalsIgnoreCase("exit")) {
                break;
            }

            switch (weekday.trim().toLowerCase()) {

                case "sunday":
                    System.out.printf("Your tasks for %s: %s\n", scedule[0][0], scedule[0][1]);
                    break;
                case "monday":
                    System.out.printf("Your tasks for %s: %s\n", scedule[1][0], scedule[1][1]);
                    break;
                case "tuesday":
                    System.out.printf("Your tasks for %s: %s\n", scedule[2][0], scedule[2][1]);
                    break;
                case "Wednesday":
                    System.out.printf("Your tasks for %s: %s\n", scedule[3][0], scedule[3][1]);
                    break;
                case "thursday":
                    System.out.printf("Your tasks for %s: %s\n", scedule[4][0], scedule[4][1]);
                    break;
                case "friday":
                    System.out.printf("Your tasks for %s: %s\n", scedule[5][0], scedule[5][1]);
                    break;
                case "saturday":
                    System.out.printf("Your tasks for %s: %s\n", scedule[6][0], scedule[6][1]);
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");

            }
        }
    }
}