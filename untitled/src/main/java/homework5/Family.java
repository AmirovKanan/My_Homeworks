package homework5;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet = new Pet();
    private int numberOfChildren;
    private int index = 0;

    public Family(Human mother, Human father, Pet pet, int numberOfChildren) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[numberOfChildren];
        this.pet = pet;
        this.numberOfChildren = numberOfChildren;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void addChild(Human child) {
        if (numberOfChildren > children.length) {
           throw new ArrayIndexOutOfBoundsException("Family is full");
        } else
            children[index++] = child;
    }

    public boolean deleteChild(int number_of_child_to_delete) {

        if (number_of_child_to_delete > this.index || number_of_child_to_delete<0)
            return false;
        else
            for (int i = number_of_child_to_delete + 1; i <= index; i++) {
                children[i-1] = children[i];
            }
        index--;
        return true;
    }

    public String Children(){
        String allChildren = "";
        for (Human child: children){
            allChildren += child.toString();
        }
        return allChildren;
    }



    public int countFamily(){
        return 2+index; // 2 is Parents count
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    // Equals and hashcode

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return numberOfChildren == family.numberOfChildren &&
                index == family.index &&
                mother.equals(family.mother) &&
                father.equals(family.father) &&
                Arrays.equals(children, family.children) &&
                pet.equals(family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet, numberOfChildren, index);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                " mother=" + mother +
                ",\nfather=" + father +
                ",\nchildren=" + Children() +
                ", \npet=" + pet +
                //", \nnumberOfChildren=" + numberOfChildren +
                // ", index=" + index +
                "}";
    }
}