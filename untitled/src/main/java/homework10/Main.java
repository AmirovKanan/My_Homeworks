package homework10;

import homework10.additional.*;
import homework10.controller.FamilyController;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        FamilyController familyController = new FamilyController();

        Man father1 = new Man("Leonardo", "DiCaprio",  LocalDate.of(1974, 02, 12).toEpochDay(), 85);
        Woman mother1 = new Woman("Camila", "Morrone", LocalDate.of(1974, 02, 12).toEpochDay(),77);
        Human child1 = new Human("Michael", "Karleone",  LocalDate.of(1998, 02, 12).toEpochDay(),77);
        Pet domesticCat = new DomesticCat("Tommy", 8, 75);

        Man father2 = new Man("Mayk", "Tayson",  LocalDate.of(1966,03,23).toEpochDay(), 85);
        Woman mother2 = new Woman("Lady", "Karleone", LocalDate.of(1965,03,13).toEpochDay(), 78);
        Human child2 = new Human("Jane", "Addison", LocalDate.of(1999,03,23).toEpochDay(), 85);
        Pet fish = new Fish("Fish", 2, 23);

        Man father3 = new Man("Mayk", "Tayson", LocalDate.of(1986,03,23).toEpochDay(), 85);
        Woman mother3 = new Woman("Addy", "Smith", LocalDate.of(1989,03,23).toEpochDay(), 78);
        Human child3 = new Human("Oliver", "Khan", LocalDate.of(2009,03,23).toEpochDay(), 85);
        Pet dog = new Dog("Max", 7, 86);

        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother2, father2);
        familyController.createNewFamily(mother3, father3);

        familyController.adoptChild(familyController.getFamilyById(0), child1);
        familyController.adoptChild(familyController.getFamilyById(1), child2);
        familyController.adoptChild(familyController.getFamilyById(2), child3);

        familyController.addPet(0,domesticCat);
        familyController.addPet(1, fish);
        familyController.addPet(2,dog);

        familyController.bornChild(familyController.getFamilyById(0), "masculine");


        System.out.println(father3.toString());
        System.out.println(father3.describeAge());
        System.out.println();

        familyController.displayAllFamilies();

        System.out.println();

        familyController.getFamiliesLessThan(2);
        familyController.getFamiliesMoreThan(1);
        familyController.countFamiliesWithMemberCount(4);
        familyController.deleteFamilyByIndex(2);
        familyController.countFamiliesWithMemberCount(4);
        familyController.getFamilyById(1);
        System.out.println(familyController.getPets(0));

    }
}

