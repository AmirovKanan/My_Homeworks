package homework10.additional;

import java.time.LocalDate;

public final class Woman extends Human {
    public Woman(String name, String surname, long birthDate, int iq){
        super(name, surname, birthDate, iq);
    }

    @Override
    public void greetPet() {
        System.out.println("I am woman, how cute is that!!");
    }

    public void makeup(){
        System.out.println("This is my makeup)");
    }
}
