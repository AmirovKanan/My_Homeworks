package homework10.additional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;

    //Creating constructor

    public Human() {
    }

    public Human(String name, String surname, long birthDate, int iq)
    {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    //Methods

    public void greetPet()
    {
        System.out.printf("Hello");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String describeAge() {
        LocalDate localBirthDate = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();

        Period period = Period.between(localBirthDate, LocalDate.now());

        return String.format("years: %d, months: %d, days: %d", period.getYears(), period.getMonths(), period.getDays());
    }

    // Equals and hashcode


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                ", iq=" + iq +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalize worked");
    }

    public long getAge() {
        return LocalDate.now().getYear() - LocalDate.ofEpochDay(birthDate).getYear();
    }
}