package homework10.additional;

import java.time.LocalDate;

public final class Man extends Human {
    public Man(String name, String surname, long birthDate, int iq){
        super(name, surname, birthDate, iq);
    }

    @Override
    public void greetPet() {
        System.out.println("I am man, I love my pet:)");
    }

    public void repairCar(){
        System.out.println("I repaired my car!!");
    }
}
