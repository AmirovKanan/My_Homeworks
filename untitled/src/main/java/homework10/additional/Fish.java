package homework10.additional;

public class Fish extends Pet {

    public Fish(String nickname, int age, int trickLevel ){
        super(nickname, age, trickLevel);
        this.setSpecies(Species.Fish);
    }

    @Override
    public void respond(String nickname) { System.out.println("My name is " + this.getNickname());
    }

    @Override
    public void foul() {

    }
}
