package homework10.additional;

public enum Species {
        Cat,
        Dog,
        Eagle,
        UNKNOWN,
        Fish,
        DomesticCat,
        RoboCat;
}
