package homework10.additional;

import java.util.ArrayList;
import java.util.List;

public class Family {
    private Woman mother;
    private Man father;
    private List<Human> children;
    private ArrayList<Pet> pets;
    private int numberOfChildren;
    private int numberOfAddedChild = 0;

    public Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
        this.pets = new ArrayList<Pet>();
        this.children = new ArrayList<>();
    }


    public void setPet(Pet pet) {
        this.pets = pets;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public Woman getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public ArrayList<Pet> getPet() {
        return pets;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void addChild(Human child) {
        children.add(child);

    }

    public String Children() {
        String allChildren = "";
        for (Human child : children) {
            allChildren += child.toString();
        }
        return allChildren;
    }

    public ArrayList<Pet> getPets() {
        return pets;
    }

    public int countFamily() {
        return 2 + children.size(); // 2 is Parents count
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Finalize worked");
    }
}