package homework9.service;

import homework9.additional.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    FamilyService familyService = new FamilyService();

    Man father1 = new Man("Leonardo", "DiCaprio", 1974, 85);
    Woman mother1 = new Woman("Camila", "Morrone", 1977, 78);
    Pet domesticCat = new DomesticCat("Tommy", 8, 75);
    Family family1;

    void before() {
        family1  = familyService.createNewFamily(father1, mother1);
        Human child1 = new Human("Michael", "Karleone", 1997, 85);
        familyService.adoptChild(family1, child1);
        familyService.addPet(0, domesticCat);
    }

    @Test
    void getAllFamilies() {
        assertNotEquals("Asdfghj", familyService.getAllFamilies());
    }

    @Test
    void count() {
        assertNotEquals(3, familyService.count());
    }

    @Test
    void createNewFamily() {

        Woman mother = new Woman("Asdfgh", "Qwertyu", 1958, 65);
        Man father = new Man("Qwerty", "Asdfg", 1956, 10);
        System.out.println(familyService.count());
        familyService.createNewFamily(father, mother);

        assertNotEquals(5, familyService.count());
    }

    @Test
    void adoptChild() {
        Family testFamily = familyService.createNewFamily(father1, mother1);
        Human child = new Human("Michael", "Karleone", 1997, 85);

        assertEquals(0, testFamily.getChildren().size());
        familyService.adoptChild(testFamily, child);
        assertEquals(1, testFamily.getChildren().size());
    }

    @Test
    void addPet() {
        family1  = familyService.createNewFamily(father1, mother1);
        Human child1 = new Human("Michael", "Karleone", 1997, 85);
        familyService.adoptChild(family1, child1);
        familyService.addPet(0, domesticCat);


        assertEquals(1, family1.getPets().size());
        familyService.addPet(0, domesticCat);
        assertEquals(2, family1.getPets().size());
    }

    @Test
    void getFamilyById() {

        assertEquals(family1, familyService.getFamilyById(0));
    }

}