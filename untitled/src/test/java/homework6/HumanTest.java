package homework6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    Human mother;

    @BeforeEach
    void before() {
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(), "Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };

        mother = new Human("Camila", "Morrone", 1977, 78, schedule);
    }

    @Test
    void getName() {
        assertEquals("Camila", mother.getName());
    }

    @Test
    void getSurname() {
        assertEquals("Morrone", mother.getSurname());
    }

    @Test
    void getYear() {
        assertNotEquals(1997, mother.getYear());
    }

    @Test
    void getIq() {
        assertEquals(78, mother.getIq());
    }

    @Test
    void getSchedule() {
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(), "Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };
        assertArrayEquals(schedule, mother.getSchedule());
    }

    @Test
    void testEquals() {
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(), "Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };
        Human mother2 = new Human("Camila", "Morrone", 1977, 78, schedule);
        assertFalse(mother.equals(mother2));
    }

    @Test
    void testHashCode() {
        assertNotEquals(564161, mother.hashCode());
    }

    @Test
    void testToString() {
        assertNotEquals("Hello0", mother.toString());
    }
}