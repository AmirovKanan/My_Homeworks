package homework6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class PetTest {
    Pet pet;
    @BeforeEach
            void before(){
        String[] habbits = {"playing", "walking", "eating"};
        pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);
    }


    @Test
    void getNickname() {
        assertEquals("Tom", pet.getNickname());
    }

    @Test
    void getSpecies() {
        assertEquals(Species.Cat, pet.getSpecies());
    }

    @Test
    void getAge() {
        assertNotEquals(34, pet.getAge());
    }

    @Test
    void getTrickLevel() {
        assertNotEquals(77, pet.getTrickLevel());
    }

    @Test
    void testEquals() {
        String[] habbits = {"playing", "walking", "eating"};
        Pet pet2 = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        assertTrue(pet.equals(pet2));
    }

    @Test
    void testHashCode() {
        assertNotEquals(656151, pet.hashCode());
    }

    @Test
    void testToString() {
        assertNotEquals("Hello World", pet.toString());
    }

}