package homework6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Family family;

    @BeforeEach
    void before(){
        String[] habbits = {"playing", "walking", "eating"};
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(), "Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };

        Human father = new Human("Leonardo", "DiCaprio", 1974, 85, schedule);
        Human mother = new Human("Camila", "Morrone", 1977, 78, schedule);
        Human child = new Human("Michael", "Karleone", 1997, 85, schedule);
        Pet pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        family = new Family(mother, father, pet, 2);
    }

    @Test
    void getMother() {
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(), "Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };
        Human mother2 = new Human("Emilie", "Clark", 1977, 78, schedule);
        assertFalse(mother2.equals(family.getMother()));

    }

    @Test
    void getFather() {  String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
            {DayofWeek.Monday.name(), "Family Time"},
            {DayofWeek.Tuesday.name(), "Gym"},
            {DayofWeek.Wednesday.name(), "Shopping"},
            {DayofWeek.Thursday.name(), "Walkig"},
            {DayofWeek.Friday.name(), "Playing football"},
            {DayofWeek.Saturday.name(), "Hanging out with friends"}
    };
        Human father2 = new Human("Jhonny", "Depp", 1968, 92, schedule);
       assertFalse(father2.equals(family.getFather()));
    }

    @Test
    void getPet() {
        String[] habbits = {"playing", "walking", "eating"};
        Pet pet2 = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        assertTrue(pet2.equals(family.getPet()));
    }

    @Test
    void addChild() {
        String[] habbits = {"playing", "walking", "eating"};
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(), "Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };

        Human father = new Human("Leonardo", "DiCaprio", 1974, 85, schedule);
        Human mother = new Human("Camila", "Morrone", 1977, 78, schedule);
        Pet pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        Human child = new Human("Michael", "Karleone", 1997, 85, schedule);
        family = new Family(mother, father, pet, 2);

        assertTrue(family.addChild(child));
    }



    @Test
    void deleteChild() {
        String[] habbits = {"playing", "walking", "eating"};
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(),"Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };

        Human father = new Human("Leonardo", "DiCaprio", 1974, 85, schedule);
        Human mother = new Human("Camila", "Morrone", 1977, 78, schedule);
        Pet pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        Human child = new Human("Michael", "Karleone", 1997, 85, schedule);
        Family family = new Family(mother, father,pet, 2);
            family.addChild(child);
        assertTrue(family.deleteChild(1));
        assertFalse(family.deleteChild(7));
    }

    @Test
    void deleteChildObject() {
        String[] habbits = {"playing", "walking", "eating"};
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(),"Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };

        Human father = new Human("Leonardo", "DiCaprio", 1974, 85, schedule);
        Human mother = new Human("Camila", "Morrone", 1977, 78, schedule);
        Pet pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        Human child = new Human("Michael", "Karleone", 1997, 85, schedule);
        Family family = new Family(mother, father,pet, 2);
        family.addChild(child);
        assertTrue(family.deleteChildObject(child));
        assertFalse(family.deleteChildObject(father));

    }


    @Test
    void countFamily() {
        assertNotEquals(5,family.countFamily());
    }

    @Test
    void testEquals() {
        String[] habbits = {"playing", "walking", "eating"};
        String[][] schedule = {{DayofWeek.Sunday.name(), "Meditation"},
                {DayofWeek.Monday.name(), "Family Time"},
                {DayofWeek.Tuesday.name(), "Gym"},
                {DayofWeek.Wednesday.name(), "Shopping"},
                {DayofWeek.Thursday.name(), "Walkig"},
                {DayofWeek.Friday.name(), "Playing football"},
                {DayofWeek.Saturday.name(), "Hanging out with friends"}
        };

        Human father3 = new Human("Leonardo", "DiCaprio", 1974, 85, schedule);
        Human mother3 = new Human("Camila", "Morrone", 1977, 78, schedule);
        Pet pet3 = new Pet(Species.Cat, "Tom", 4, 90, habbits);
        Family family3 = new Family(mother3, father3, pet3, 2);
        assertFalse(family.equals(family3));
    }


    @Test
    void testHashCode() {
        assertNotEquals(52454, family.hashCode());
    }

    @Test
    void testToString() {
        String expected = "Family{ mother=Human{name='Camila', surname='Morrone', year=1977, iq=78, schedule={Sunday---Meditation}{Monday---Family Time}{Tuesday---Gym}{Wednesday---Shopping}{Thursday---Walkig}{Friday---Playing football}{Saturday---Hanging out with friends}},\n" +
                "father=Human{name='Leonardo', surname='DiCaprio', year=1974, iq=85, schedule={Sunday---Meditation}{Monday---Family Time}{Tuesday---Gym}{Wednesday---Shopping}{Thursday---Walkig}{Friday---Playing football}{Saturday---Hanging out with friends}}, \n" +
                "pet=Pet{species='Cat', nickname='Tom', age=4, trickLevel=90, habits=[playing, walking, eating]}}";

        assertEquals(expected, family.toString());
    }

}