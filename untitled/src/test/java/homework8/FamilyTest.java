package homework8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    Family family;
    Map<String, String> schedule = new HashMap<String, String>();

    {
        schedule.put(DayofWeek.Monday.name(), "Family Time");
        schedule.put(DayofWeek.Tuesday.name(), "Gym");
        schedule.put(DayofWeek.Wednesday.name(), "Shopping");
        schedule.put(DayofWeek.Thursday.name(), "Walkig");
        schedule.put(DayofWeek.Friday.name(), "Playing football");
        schedule.put(DayofWeek.Saturday.name(), "Hanging out with friends");
    }

    Human child = new Human("Michael", "Karleone", 1997, 85, schedule);

    @BeforeEach
    void before() {
        Map<String, String> schedule = new HashMap<String, String>();

        schedule.put(DayofWeek.Monday.name(), "Family Time");
        schedule.put(DayofWeek.Tuesday.name(), "Gym");
        schedule.put(DayofWeek.Wednesday.name(), "Shopping");
        schedule.put(DayofWeek.Thursday.name(), "Walkig");
        schedule.put(DayofWeek.Friday.name(), "Playing football");
        schedule.put(DayofWeek.Saturday.name(), "Hanging out with friends");


        Set<String> habitsOfFish = new HashSet<String>();
        habitsOfFish.add("eating");
        habitsOfFish.add("swim");

        Set<String> habitsOfDomesticCat = new HashSet<String>();
        habitsOfDomesticCat.add("playing");
        habitsOfDomesticCat.add("walking");
        habitsOfDomesticCat.add("eating");

        Set<String> habitsOfDog = new HashSet<String>();
        habitsOfDog.add("playing");
        habitsOfDog.add("eating");
        habitsOfDog.add("barking");

        Set<String> habitsOfRoboCat = new HashSet<String>();
        habitsOfRoboCat.add("speaking");
        habitsOfRoboCat.add("walking");


        Pet dog = new Dog("Max", 8, 81, habitsOfDog);
        Pet domesticCat = new DomesticCat("Tom", 9, 87, habitsOfDomesticCat);
        Pet fish = new Fish("Red", 1, 25, habitsOfFish);
        Pet roboCat = new RoboCat("RoboTom", 4, 100, habitsOfRoboCat);

        Man father = new Man("Leonardo", "DiCaprio", 1974, 85, schedule);
        Woman mother = new Woman("Camila", "Morrone", 1977, 78, schedule);
        //  Pet pet = new Pet(Species.Cat, "Tom", 4, 90, habbits);

        ArrayList<Pet> pets = new ArrayList<Pet>();
        pets.add(dog);
        pets.add(domesticCat);
//        pets.add(fish);
//        pets.add(roboCat);

        family = new Family(mother, father, 2, pets);
    }

    @Test
    void addChild() {
        assertTrue(family.addChild(child));

    }

    @Test
    void countFamily() {
      //  assertEquals(2, family.countFamily());
        family.addChild(child);
        assertEquals(3, family.countFamily());
    }
}